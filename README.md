Description
===========

This is a Hex Game simulatior using C++ 11.


The program simulates a Blue/Red player and allows to play against the CPU.

The game uses a solution tree using monte carlo to help in playing and makes use of threads in applicable to speed up the processing. 

Compiling
=========

* Run `g++ game.cpp -std=c++11 -ogame`
* Run `./game`
